﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System;
using Xunit;
using FluentAssertions;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        
        private readonly PartnersController _partnersController;

        private readonly IFixture _fixture;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = _fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = _fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            return partner;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = _fixture.Build<Partner>()
                .With(x => x.IsActive, false)
                .Without(x => x.PartnerLimits).Create();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            
            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitExpired_ReturnsObj()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 5;
            var newLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.CancelDate, DateTime.Now)
                .With(x => x.Partner, partner)
                .Create();
            partner.PartnerLimits.Add(newLimit);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var result = await _partnersRepositoryMock.Object.GetByIdAsync(partnerId);
            // Assert
            
            result.NumberIssuedPromoCodes.Should().Be(5);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_ZeroPromocodes_ReturnsObj()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes = 5;
            var newLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.Partner, partner)
                .Without(x => x.CancelDate)
                .Create();
            partner.PartnerLimits.Add(newLimit);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var result = await _partnersRepositoryMock.Object.GetByIdAsync(partnerId);
            // Assert

            result.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PreviousLimitOff_ReturnsObj()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();
            var newLimit = _fixture.Build<PartnerPromoCodeLimit>()
                .With(x => x.Partner, partner)
                .Without(x => x.CancelDate)
                .Create();
            partner.PartnerLimits.Add(newLimit);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var result = await _partnersRepositoryMock.Object.GetByIdAsync(partnerId);
            // Assert

            result.PartnerLimits.FirstOrDefault(x => x.CancelDate.HasValue)
                .Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitbelowZero_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = _fixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, -2).Create();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            // Assert

            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SaveInBd_ReturnsObj()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = CreateBasePartner();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = _fixture.Create<SetPartnerPromoCodeLimitRequest>();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);
            var result = await _partnersRepositoryMock.Object.GetByIdAsync(partnerId);
            // Assert
            result.PartnerLimits.FirstOrDefault(x => !x.CancelDate.HasValue)
                .Should().NotBeNull();
        }
    }
}